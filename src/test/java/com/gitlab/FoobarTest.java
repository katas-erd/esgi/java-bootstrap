package com.gitlab;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class FoobarTest {
    @Test
    @DisplayName("This test should pass")
    void test1() {
        assertThat(2 + 2).isEqualTo(4);
    }

    @Test
    @DisplayName("This test should pass also")
    void test2() {
        assertThat(2 == 2).isTrue();
    }
}
