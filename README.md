# Java Maven starter pack

## Support de cours & rappels

Vous pouvez retrouver les slides [ici](https://crazy-crafters.gitlab.io/red-maple/)


## Structure

- Le code source doit être dans le répertoire `src/main/java`. De préférence dans le package `com.gitlab`
- Les tests doivent être dans le répertoire `src/test/java`. Ne pas oublier de mettre le tests dans les packages correspondant à ceux du code qu'ils valident

## Lancer les tests avec couverture de code
```
mvn clean verify
```

Le rapport sera généré [ici](./target/site/jacoco/index.html)

